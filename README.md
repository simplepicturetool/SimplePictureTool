# SimplePictureTool
SimplePictureTool is a simple GUI that using ImageMagick and imgp for batch processing of .PNG or .JPG image files.

#### Features of the GUI: 
 * Allows to compress all image files in a directory.
 * Allows to reduce the image-size.
 * Allows to remove META-Data from image files.

#### Requierment: 
* Qt 5.14 (or higher) - Download Qt here: https://www.qt.io/download
* ImageMagick - https://imagemagick.org/index.php
* imgp - https://github.com/jarun/imgp

#### Get the latest build of SimplePictureTool for Linux:
<a href="https://gitlab.com/simplepicturetool/SimplePictureTool/-/releases">https://gitlab.com/simplepicturetool/SimplePictureTool/-/releases</a>

#### How to build the Application:

```
qmake (or qmake-qt5 / qmake5)
make -f Makefile
``` 

#### Screenshot:
![](screenshot.png)
